<p align="center">
  <h1>Zeroes - 5CS027</h1>
  <a href="https://github.com/JoshLmao/5CS027-Zeroes">
    <img src="https://img.shields.io/badge/github-Zeroes-turquoise.svg?style=flat-square.svg"/>
  </a>
  <a href="https://twitter.com/JoshLmao">
    <img src="https://img.shields.io/badge/twitter-JoshLmao-blue.svg?style=flat-square.svg"/>
  </a>
</p>

## About

Top down Player vs Enemy, hero-based game where you fight through enemies to defeat the evil Aura!

Built inside Unreal Engine 4.22.3, using hand made assets and Paragon model assets:

- Aura ([Aurora](https://www.unrealengine.com/marketplace/en-US/product/paragon-aurora))
- Daoko ([Shinbi](https://www.unrealengine.com/marketplace/en-US/product/paragon-shinbi))
- Enemies ([Paragon Minions](https://www.unrealengine.com/marketplace/en-US/product/paragon-minions))
- Fantasy Music ([Fantasy Orchestral Music](https://www.unrealengine.com/marketplace/en-US/product/fantasy-orchestral-music))